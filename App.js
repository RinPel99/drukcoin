import { StyleSheet, Text, View, LogBox } from 'react-native';
import { NavigationContainer } from '@react-navigation/native';

import StackNavigation from './navigators/Stack';

LogBox.ignoreAllLogs();

export default function App() {
  return (
      <NavigationContainer>
        
        <StackNavigation/>
        
      </NavigationContainer>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
