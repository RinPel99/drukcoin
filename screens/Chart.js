import React, { useState, useEffect } from 'react';
import { View, Text, StyleSheet } from 'react-native';
import { Svg, Rect, Text as SvgText, Line } from 'react-native-svg';
import { Picker } from '@react-native-picker/picker';
import {getAuth, createUserWithEmailAndPassword,signInWithEmailAndPassword,signOut} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs,  } from '@firebase/firestore';
import { db } from '../config';
import {firebase} from '../config';

const ChartPage = () => {
  const [selectedYear, setSelectedYear] = useState(new Date().getFullYear());
  const [data, setData] = useState([]);
  const [years, setYears] = useState([]);

  const auth = getAuth()
  const today = new Date();
  const user = auth.currentUser;
  let month = today.getMonth() + 1;
  let year = today.getFullYear();

  const [expenseData, setItemArray2] = useState('');
  const itemListCollection2 = firebase.firestore().collection('expense');

  // Array of colors for each month
  const barColors = [
    '#FF0000',
    '#00FF00',
    '#0000FF',
    '#FFFF00',
    '#FF00FF',
    '#00FFFF',
    '#800000',
    '#008000',
    '#000080',
    '#808000',
    '#800080',
    '#008080',
  ];
  

  useEffect(() => {
    // Fetch data for the selected year
    fetchData(selectedYear);
  }, [selectedYear]);

  useEffect(() => {
    let start = "2019";
    let years =[];
    for (let index = Number(start); index <= Number(year); index++) {
      years.push(String(index))
      console.log(years)
    }
    setYears(years)
  }, []);

  useEffect ( () => {
    var cleanup = itemListCollection2
    .onSnapshot(
      querySnapshot => {
        const itemArray = []
        querySnapshot.forEach((doc) => {
          const {day,month,time,user,expenseName,expenseAmount,expenseDes} = doc.data()
          itemArray.push({
            id: doc.id,
            day,
            month,
            year,
            time,
            user,
            expenseName,
            expenseAmount,
            expenseDes
          })
        })
        setItemArray2(itemArray)
      })
    
    return cleanup;
  },[])

  const fetchData = (year) => {
    // TODO: Fetch data for the selected year and update the "data" state
    var data3 = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];

    for (let i = 0; i < expenseData.length; i++) {
        if(user.uid == expenseData[i].user){
          
            if(expenseData[i].year == selectedYear){

                // expenseTotal = expenseTotal + Number(expenseData[i].expenseAmount)
                data3[expenseData[i].month] = data3[expenseData[i].month] + Number(expenseData[i].expenseAmount)
                console.log(expenseData[i].month)
            }
        }
    }
    // Replace the following dummy data with your actual data retrieval logic
    console.log(data3)
    setData(data3);
  };

  // Calculate the maximum value in the data array
  const maxValue = Math.max(...data);

  // Array of month labels
  const months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];

  // Calculate the width of each bar based on the available space
  const barWidth = 25;
  const chartWidth = data.length * barWidth;
  const marginLeft = 25; // Adjust the margin as needed

  // Calculate the height of the chart area
  const chartHeight = 250;

  // Calculate the height of the axes
  const xAxisHeight = 20;
  const yAxisWidth = 40;

  const handleYearChange = (year) => {
    setSelectedYear(year);
  };


  return (
    <View style={styles.container}>
      <View style={styles.nav}>
                <Text style={{textAlign:'center',fontSize:22,
                    fontWeight:"500",color:'white',}}>Notifications </Text>
            </View>

          <View style={styles.pickerContainer}>
            <Text style={styles.pickerLabel}>Select Year:</Text>
            <Picker
              style={styles.picker}
              selectedValue={selectedYear}
              onValueChange={handleYearChange}
            >
               {years.map((year) => (
                  <Picker.Item label={year} value={year} key={year} />
                ))}
                
            </Picker>
          </View>

      <Text style={styles.title}>Bar Chart for {selectedYear}</Text>

      <Svg width="100%" height={chartHeight + xAxisHeight + 20}>
        {/* Draw x-axis */}
        <Line
          x1={marginLeft}
          y1={chartHeight}
          x2={marginLeft + chartWidth}
          y2={chartHeight}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw y-axis */}
        <Line
          x1={marginLeft}
          y1={0}
          x2={marginLeft}
          y2={chartHeight}
          stroke="black"
          strokeWidth="1"
        />

        {/* Draw bars */}
        {data.map((value, index) => (
          <Rect
            key={index}
            x={index * barWidth + marginLeft + 5} // Add 5 units of space between bars
            y={chartHeight - (value / maxValue) * chartHeight}
            width={barWidth - 10} // Reduce the width to accommodate the space
            height={(value / maxValue) * chartHeight}
            fill={barColors[index % 12]}
          />
        ))}

        {months.map((month, index) => (
          <SvgText
            key={index}
            x={index * barWidth + marginLeft + barWidth / 2}
            y={chartHeight + xAxisHeight - 5} // Adjust the position of the label as needed
            fill="black"
            fontSize="12"
            textAnchor="middle"
          >
            {month}
          </SvgText>
        ))}

        {/* Draw y-axis labels */}
        <SvgText
          x={marginLeft - 10} // Adjust the position of the label as needed
          y={chartHeight}
          fill="black"
          fontSize="12"
          textAnchor="end"
        >
          {maxValue}
        </SvgText>
      </Svg>
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 5,
  },
  nav: {
    height: 150,
    backgroundColor: '#187B75',
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    gap: 85,
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 10,
    marginBottom:10,
  },
  pickerContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
  },
  pickerLabel: {
    fontSize: 16,
    fontWeight: '500',
    marginRight: 10,
  },
  picker: {
    flex: 1,
    backgroundColor: '#187B75',
    color: 'black',
    borderRadius: 10,
    color: 'white',
  },
  title: {
    fontSize: 16,
    fontWeight: '500',
    marginBottom: 10,
    color: 'black',
  },
});

export default ChartPage;
