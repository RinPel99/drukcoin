import { View,Text,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView, TextInput,} from 'react-native';
import {getAuth, createUserWithEmailAndPassword,signInWithEmailAndPassword,} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs,  } from '@firebase/firestore';
import { db } from '../config';
import React, { useState, useEffect} from "react";
import {firebase} from '../config'



function ChartPage( {navigation}){

    const handleMonthNotification=()=>{
      navigation.navigate("SetBudget")
    }
  
  const auth = getAuth()
  const today = new Date();
  const user = auth.currentUser;
  let month = today.getMonth() + 1;
  let year = today.getFullYear();

  const [data, setItemArray] = useState('');
  const [data2, setItemArray2] = useState('');
  const itemListCollection1 = firebase.firestore().collection('budget');
  const itemListCollection2 = firebase.firestore().collection('expense');

  useEffect ( () => {
      itemListCollection1
      .onSnapshot(
        querySnapshot => {
          const itemArray = []
          querySnapshot.forEach((doc) => {
            const {budget,month,user,year} = doc.data()
            itemArray.push({
              id: doc.id,
              user,
              budget,
              month,
              year
            })
          })
          setItemArray(itemArray)
        })
    },[])
  let budget

  useEffect ( () => {
      itemListCollection2
      .onSnapshot(
        querySnapshot => {
          const itemArray = []
          querySnapshot.forEach((doc) => {
            const {day,month,time,user,expenseName,expenseAmount,expenseDes} = doc.data()
            itemArray.push({
              id: doc.id,
              day,
              month,
              year,
              time,
              user,
              expenseName,
              expenseAmount,
              expenseDes
            })
          })
          setItemArray2(itemArray)
        })
    },[])

  let expenseTotal =Number("0");

  for (let i = 0; i < data.length; i++) {
      if(user.uid == data[i].user){
          if(month==data[i].month && year==data[i].year){
              budget = Number(data[i].budget);
          }
      }
  }

  for (let i = 0; i < data2.length; i++) {
      if(user.uid == data2[i].user){
          if(month==data2[i].month && year==data2[i].year){
              expenseTotal = expenseTotal + Number(data2[i].expenseAmount)
          }
      }
  }
  // let fifty
  // let ninty

  // if ((expenseTotal/budget)>=0.9) {
  //     ninty=90;
  // }
  // else if((expenseTotal/budget)>=0.5){
  //     fifty=50;
  // }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={styles.nav}>
                <Text style={{textAlign:'center',fontSize:22,
                    fontWeight:"500",color:'white',}}>Notifications </Text>
            </View>
            <View >
            {!budget&&<TouchableOpacity style={styles.SetBudgetButton} onPress={handleMonthNotification}>
            <View style={{backgroundColor:"#D9D9D9",height:62,justifyContent:"center",alignItems:'center',
                    borderRadius:10
                    }}>
                    <Text style={{color:'white',fontSize:17}}>You need to set a budget for this month</Text>
                    </View>
                </TouchableOpacity>}
                {/* {ninty&&<View style={{backgroundColor:"#D9D9D9",height:62,justifyContent:"center",alignItems:'center',
                    borderRadius:10
                    }}>
                    <Text style={{color:'black',fontSize:17}}>You have use 90% or more of your budget for this month</Text>
                </View>}
                {fifty&&<View style={{backgroundColor:"#D9D9D9",height:62,justifyContent:"center",alignItems:'center',
                    borderRadius:10
                    }}>
                    <Text style={{color:'black',fontSize:17}}>You have use 50% or more of your budget for this month</Text>
                </View>} */}
            </View>
        
        </KeyboardAvoidingView>

    )
}
export default ChartPage;

const styles=StyleSheet.create({
    container: {
        flex: 1,
    },
    nav: {
      height: 150,
      backgroundColor: '#187B75',
      display: 'flex',
      flexDirection: 'row',
      justifyContent: 'center',
      alignItems: 'center',
      gap: 85,
      borderBottomLeftRadius: 20,
      borderBottomRightRadius: 20,
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation: 10,
    },
    form:{
      marginTop:"5%",
      justifyContent:"center",
      alignItems:"center",
    },
    field: {
      flexDirection: 'row',
      alignItems: 'center',
      marginBottom: 10,
      borderWidth:1,
      borderRadius:20
    },
    input: {
      flex: 1,
      borderColor: '#888',
      paddingVertical: 8,
      paddingHorizontal: 10,
    },
    icon: {
      marginLeft: 10,
    },
    editIcon: {
      marginRight: 10,
    },
    saveButton: {
      backgroundColor: '#187B75',
      paddingVertical: 10,
      paddingHorizontal: 20,
      alignSelf: 'flex-end',
      borderRadius: 5,
      marginRight:20,marginTop:5
    },
    saveIcon: {
      alignSelf: 'center',
    },
    monthNotification:{
      borderWidth:1,
      width:"90%",
      marginLeft:"5%",
      padding:"5%",
      borderRadius:20,
      marginTop:"5%",
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation:30,
    }

})