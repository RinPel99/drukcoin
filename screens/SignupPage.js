import { useState } from 'react';
import HeaderPage from '../components/header';
import { View,Text,TextInput,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView, Platform  } from 'react-native';
// import { Icon } from '@expo/vector-icons';
import { firebase } from '../config';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function Signup({navigation}){
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [confirmPassword, setConfirmPassword] = useState('');
    const [nameError,setNameError]=useState(null);
    const [emailError, setEmailError] = useState(null);
    const [passwordError, setPasswordError] = useState(null);
    const [confirmError,setConfirmError]=useState(null);


    const handleSignup = async()=>{
        let valid = true;

    // Name validation
    if (!name) {
      setNameError('Please enter your name.');
      valid = false;
    } else {
      setNameError(null);
    }

    // Email validation
    if (!email) {
      setEmailError('Please enter your email address.');
      valid = false;
    } else if (!/\S+@\S+\.\S+/.test(email)) {
      setEmailError('Please enter a valid email address.');
      valid = false;
    } else {
      setEmailError(null);
    }

    // Password validation
    if (!password) {
      setPasswordError('Please enter a password.');
      valid = false;
    } else if (password.length < 6) {
      setPasswordError('Password must be at least 6 characters long.');
      valid = false;
    } else {
      setPasswordError(null);
    }

    // Confirm password validation
    if (!confirmPassword) {
      setConfirmError('Please confirm your password.');
      valid = false;
    } else if (confirmPassword !== password) {
      setConfirmError('Passwords do not match.');
      valid = false;
    } else {
      setConfirmError(null);
    }
    if (valid===true){
      await firebase.auth().createUserWithEmailAndPassword(email,password)
      .then(()=>{
          firebase.auth().currentUser.sendEmailVerification({
            handleCodeInApp: true,
            url:'https://mobile-application-5aeaf.firebaseapp.com',
      
          })
          .then(()=>{
              alert("verification email set")
          })
          .catch((error)=>{
              alert(error.message)
          })
          .then(()=>{
              firebase.firestore().collection('users')
              .doc(firebase.auth().currentUser.uid)
              .set({
                  name,
                  email,
              })
              navigation.navigate('Login');
          })
          .catch((error)=>{
              alert(error.message)
          })
      })
      .catch((error)=>{
          console.log(error.message)
          alert(error.message)
      }) 
        
    }
    return valid;

    }

    const handleLogin = () => {
        // handle login logic

        navigation.navigate("Login")
    }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <HeaderPage/>
            <View style={styles.form}>
                <Text style={{textAlign:'center',fontSize:27,fontWeight:'500'}}>Resigter</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Name"
                    placeholderTextColor="#888"
                    onChangeText={text => setName(text)}
                    value={name}
                />
                {nameError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{nameError}</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="Email"
                    placeholderTextColor="#888"
                    onChangeText={text => setEmail(text)}
                    value={email}
                />
                {emailError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{emailError}</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    placeholderTextColor="#888"
                    secureTextEntry
                    onChangeText={text => setPassword(text)}
                    value={password}
                />
                {passwordError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{passwordError}</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="Confirm Password"
                    placeholderTextColor="#888"
                    secureTextEntry
                    onChangeText={text => setConfirmPassword(text)}
                    value={confirmPassword}
                />
                {confirmError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{confirmError}</Text>}
                <TouchableOpacity style={styles.button} onPress={()=>handleSignup()}>
                    <Text style={styles.buttonText}>Sign Up</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.signupButton} onPress={handleLogin}>
                    <Text style={styles.signupButtonText}>Already have an account? Log In</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>

    )
}
export default Signup;

const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white',
    },
    form:{
        marginTop:"-6%",
        paddingTop:30,
        backgroundColor:'#FFFFFF',
        borderTopStartRadius:30,
        borderTopEndRadius:30,
        width: '100%',
        alignItems: 'center',
    },
    input: {
        paddingVertical: windowHeight * 0.015,
        paddingHorizontal: windowWidth * 0.05,
        borderRadius: 10,
        marginTop: windowHeight * 0.015,
        width: windowWidth * 0.8,
        fontSize: windowWidth * 0.04,
        color: '#333',
        borderColor: '#ccc',
        borderWidth: 1,
    },
    button: {
        backgroundColor: '#187B75',
        padding: windowHeight * 0.016,
        borderRadius: 5,
        marginTop: windowHeight * 0.05,
        minWidth: windowWidth * 0.8,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: windowWidth * 0.05,
        textAlign: 'center',
    },
    signupButton: {
        marginTop: windowHeight * 0.05,
    },
    signupButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
    },
    forgotButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
        marginTop:-20,
    },
})