import { View,Text,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView } from 'react-native';
// import { Icon } from '@expo/vector-icons';
import Icon from 'react-native-vector-icons/Ionicons';
import Speedometer from './speed';
import {getAuth, createUserWithEmailAndPassword,signInWithEmailAndPassword,signOut} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs,  } from '@firebase/firestore';
import { db } from '../config';
import { useState,useEffect} from "react";
import {firebase} from '../config';

function HomePage({navigation}){
    const auth = getAuth()
    const today = new Date();
    const user = auth.currentUser;
    let month = today.getMonth() + 1;
    let year = today.getFullYear();

    const navExpense=(value)=>{
        let itemId=value.id
        navigation.navigate('ExpenseDetails',{itemId})
    }

    const [data, setItemArray] = useState('');
    const [data2, setItemArray2] = useState('');
    const itemListCollection1 = firebase.firestore().collection('budget');
    const itemListCollection2 = firebase.firestore().collection('expense');

    useEffect ( () => {
        var cleanup = itemListCollection1
        .onSnapshot(
          querySnapshot => {
            const itemArray = []
            querySnapshot.forEach((doc) => {
              const {budget,month,user,year} = doc.data()
              itemArray.push({
                id: doc.id,
                user,
                budget,
                month,
                year
              })
            })
            setItemArray(itemArray)
          })
        
          return cleanup;
      },[])
    let budget

    useEffect ( () => {
        var cleanup = itemListCollection2
        .onSnapshot(
          querySnapshot => {
            const itemArray = []
            querySnapshot.forEach((doc) => {
              const {day,month,time,user,expenseName,expenseAmount,expenseDes} = doc.data()
              itemArray.push({
                id: doc.id,
                day,
                month,
                year,
                time,
                user,
                expenseName,
                expenseAmount,
                expenseDes
              })
            })
            setItemArray2(itemArray)
          })
        
        return cleanup;
      },[])

    let expenseTotal = Number("0");

    for (let i = 0; i < data.length; i++) {
        if(user.uid == data[i].user){
            if(month==data[i].month && year==data[i].year){
                budget = data[i].budget;
            }
        }
    }
    var data3 = [];

    for (let i = 0; i < data2.length; i++) {
        if(user.uid == data2[i].user){
            data3.push(data2[i]);
            if(month==data2[i].month && year==data2[i].year){
                expenseTotal = expenseTotal + Number(data2[i].expenseAmount)
            }
        }
    }

    const moneyUsed = expenseTotal; // Example value for money used
    const totalMoney = budget; // Example value for total available money

    const handleSeeAll=()=>{
        navigation.navigate("MyExpense")
    }
    
    const handleAddExpense=()=>{
        
        if(budget) {
            // console.log(value);
            navigation.navigate('AddExpense', {value:'Expense name'});
        }else{
            alert("You need to set a Budget first!!")
            navigation.navigate('SetBudget');
        }
        
    }
    const handleAddRechargeExpense=()=>{
        
        if(budget) {
            navigation.navigate('AddExpense', {value:"Phone Recharge"});
        }else{
            alert("You need to set a Budget first!!")
            navigation.navigate('SetBudget');
        }
        
    }
    const handleAddBillExpense=()=>{
        
        if(budget) {
            navigation.navigate('AddExpense', {value:"Bills"});
        }else{
            alert("You need to set a Budget first!!")
            navigation.navigate('SetBudget');
        }
        
    }

    return (
        <KeyboardAvoidingView style={styles.container}>
            <View style={styles.nav}>
              
                <Text style={{textAlign:'center',fontSize:20,
                    fontWeight:"500",color:'white',}}>Budget: Nu.{budget?budget:"00.00"}
                </Text>
            </View>
            <View style={styles.budgetContainer}>
            {budget&&<Speedometer value={moneyUsed} total={totalMoney} />}
            {!budget&&<Text>Set a budget first!</Text> }
            </View>
            
            <View style={styles.AddExpenseTitle}>
                <Text style={{fontWeight:'500',fontSize:22}}>Add Expense</Text>
            </View>
            <View style={styles.AddExpenseIcon}>
                <TouchableOpacity style={styles.Mobile} onPress={handleAddRechargeExpense}>
                    <Text style={styles.Call}>
                        <Icon name="ios-call" size={40} color="grey" />
                    </Text>
                    <View >
                        <Text style={{textAlign:'center',color:'white'}}>Recharge</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.BillPayment} onPress={handleAddBillExpense}>
                    <Text style={styles.bill}>
                        <Icon name="md-card" size={40} color="grey" />
                    </Text>
                    <View >
                        <Text style={{textAlign:'center',color:'white'}}>Bills</Text>
                    </View>
                </TouchableOpacity>
                <TouchableOpacity style={styles.Plus} onPress={handleAddExpense}>
                    <Text style={styles.add}>
                        <Icon name="ios-add" size={40} color="grey" />
                    </Text>
                    <View >
                        <Text style={{textAlign:'center',color:'white'}}>Others</Text>
                    </View>
                </TouchableOpacity>
            </View>
            <View style={styles.Myexpense}>
                <Text style={{fontSize:22,fontWeight:"500"}}>My Expense</Text>
                <TouchableOpacity style={styles.SeeAllContainer} onPress={handleSeeAll}>
                    <Text style={{fontSize:16,fontWeight:'500',color:'white'}}>
                        SeeAll
                    </Text>
                </TouchableOpacity>
            
            </View>
            {!data3[0]&&<View style={styles.Expenselist}>
                <Text>You haven't made any Expense!</Text>
            </View>}
            {data3[0]&&<TouchableOpacity onPress={() => {
                    navExpense(data3[0])
                }}>
            <View style={styles.Expenselist}>
                <Text style={styles.expenseName}>{data3[0].expenseName}</Text>
                <Text style={styles.expensePrice}>{data3[0].expenseAmount}</Text>
            </View>
            </TouchableOpacity>}
            {data3[1]&&<TouchableOpacity onPress={() => {
                    navExpense(data3[1])
                }}>
            <View style={styles.Expenselist}>
                <Text style={styles.expenseName}>{data3[1].expenseName}</Text>
                <Text style={styles.expensePrice}>{data3[1].expenseAmount}</Text>
            </View>
            </TouchableOpacity>}
        

           
        </KeyboardAvoidingView>

    )
}
export default HomePage;

const styles=StyleSheet.create({
    container: {
        flex: 1,
        // backgroundColor:"#429690"
    },
    nav:{
        // borderWidth:1,
        height:150,
        backgroundColor:'#187B75',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:85,
        borderBottomLeftRadius:20,
        borderBottomRightRadius:20
        // marginTop:20
        
    },
    sidebarMenu:{
        // borderWidth:1,
        justifyContent:'center',
        alignItems:'center'
    },

    budgetContainer:{
        marginTop:"15%",
        width:"80%",
        marginLeft:"10%",
        height:"25%",
        borderRadius:19,
        marginTop:22,
        backgroundColor:'#C8C8C8',
        justifyContent:'center',
        alignItems:'center',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30,
        marginTop:"-10%",
    },
    AddExpenseTitle:{
        // borderWidth:1,
        marginTop:'15%',
        width:'90%',
        marginLeft:'5%'
    },
    AddExpenseIcon:{
        // borderWidth:1,
        marginTop:'5%',
        width:'90%',
        marginLeft:'5%',
        display:'flex',
        flexDirection:'row',
        gap:15
    },
    Mobile:{
        // borderWidth:1,
        borderRadius:5,
        backgroundColor:"#2D3648",
        height:64,
        width:67,
        justifyContent:'center',
        alignItems:'center',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30
    },
    BillPayment:{
        // borderWidth:1,
        borderRadius:5,
        backgroundColor:"#2D3648",
        height:64,
        width:67,
        justifyContent:'center',
        alignItems:'center',
        display:'flex',
        flexDirection:'column',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30
        
    },
    Plus:{
        // borderWidth:1,
        borderRadius:5,
        backgroundColor:"#2D3648",
        height:64,
        width:67,
        justifyContent:'center',
        alignItems:'center',
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30
    },
    Myexpense:{
        // borderWidth:1,
        width:'90%',
        marginLeft:'5%',
        marginTop:'5%',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:140
    },
    SeeAllContainer:{
        // borderWidth:1,
        justifyContent:'center',
        alignItems:'center',
        width:91,
        height:32,
        borderRadius:16,
        backgroundColor:'#187B75',
    },
    Expenselist:{
        // borderWidth:1,
        marginTop:'15%',
        height:62,
        width:'90%',
        marginLeft:'5%',
        borderRadius:10,
        backgroundColor:'#C8C8C8',
        display:'flex',
        flexDirection:'row',
        justifyContent:'center',
        alignItems:'center',
        gap:130,
        shadowColor: 'black',
        shadowOffset: { width: 0, height: 2 },
        shadowOpacity: 1,
        shadowRadius: 2,
        elevation:30
    },
    expenseName:{
        fontSize:18,
        fontWeight:'500'
    },
    expensePrice:{
        fontSize:18,
        fontWeight:'400'
    },
    
})