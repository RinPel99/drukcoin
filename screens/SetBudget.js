import { View,StyleSheet,Text,TouchableOpacity,TextInput,Dimensions,Alert } from "react-native";
import HeaderPage from "../components/header";
import {getAuth} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { db } from '../config';
import { useState } from "react";
import {firebase} from '../config'

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;


function BudgetSetPage( {navigation }){
    const [budget, setBudget] = useState('');


    const prompt = () =>
    Alert.alert('Are you sure?', 'are you sure you want to set the budget for this month to', [
      {
        text: 'Yes',
        onPress: () => handleStartButton(budget),
      },
      {text: 'No'},
    ]);

    
    const auth = getAuth()
    const today = new Date();
    const user = auth.currentUser;

    let month = today.getMonth() + 1;
    let year = today.getFullYear();


    const handleStartButton= async(budget)=>{

        try {
            firebase.firestore().collection('budget')
            .doc()
            .set({
                month:month,
                year:year,
                user:user.uid,
                budget:budget,
            })
            alert("Budget is set") 
            navigation.navigate('Tab');

        } catch (error) {
            console.error('Error adding document: ', error);
        }
                
    }
    const handleBudgetChange = (text) => {
        setBudget(text);
    };
    return(
        <View style={styles.container}>
            <HeaderPage/>
            <View style={styles.form}>
                <Text style={{textAlign:'center',fontSize:27,fontWeight:'500'}}>Set Budget</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Enter total amount"
                    placeholderTextColor="#888"
                    onChangeText={handleBudgetChange}
                    value={budget}
                />
                <TouchableOpacity style={styles.button} onPress={prompt}>
                    <Text style={styles.buttonText}>Start</Text>
                </TouchableOpacity>
            </View>
        </View>
    )
}
export default BudgetSetPage;

const styles=StyleSheet.create({
    container:{
        flex:1,
    },
    form:{
        marginTop:"-6%",
        paddingTop:30,
        backgroundColor:'#FFFFFF',
        borderTopStartRadius:30,
        borderTopEndRadius:30,
        width: '100%',
        alignItems: 'center',
    },
    input: {
        paddingVertical: windowHeight * 0.015,
        paddingHorizontal: windowWidth * 0.05,
        borderRadius: 10,
        marginTop: windowHeight * 0.03,
        width: windowWidth * 0.8,
        fontSize: windowWidth * 0.04,
        color: '#333',
        borderColor: '#ccc',
        borderWidth: 1,
    },
    button: {
        backgroundColor: '#187B75',
        padding: windowHeight * 0.016,
        borderRadius: 5,
        marginTop: windowHeight * 0.05,
        minWidth: windowWidth * 0.8,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: windowWidth * 0.05,
        textAlign: 'center',
    },

})