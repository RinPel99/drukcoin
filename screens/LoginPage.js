import { useState } from 'react';
import HeaderPage from '../components/header';
import { View,Text,TextInput,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView } from 'react-native';
// import { Icon } from '@expo/vector-icons';
import { firebase } from "../config"

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function Login({navigation}){
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailError, setEmailError] = useState(null);
    const [passwordError, setPasswordError] = useState(null);

    // form validaition

    const validateForm = async ()=>{
        let valid=true;

        //validate email
        if(!email){
            setEmailError("Email is required");
            valid=false;
        }

        else {
            setEmailError(null);
        }

        //password validation
        if (!password) {
            setPasswordError("Password is required");
            valid = false;
        } 

        else {
            setPasswordError(null);
        }
        if(valid===true){
            try{
                if(await firebase.auth().signInWithEmailAndPassword(email,password) && firebase.auth().currentUser.emailVerified){
                  setEmail("")
                  setPassword("")
                  navigation.navigate("Tab")
                }
                else if (!firebase.auth().currentUser.emailVerified){
                    alert("Email not Verified")
                }

                
              }catch(error){
                alert(error.message)
              }
        }
        return valid;
        
    }
    
    const handleForgotPassword = () => {
    // handle forgot password logic
        navigation.navigate("ForgotPassword")
    }
    const handleSignup=()=>{
    // handle signup
        navigation.navigate("Signup")
    }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <HeaderPage/>
            <View style={styles.form}>
                <Text style={{textAlign:'center',fontSize:27,fontWeight:'500'}}>Sign In</Text>
                <TextInput
                    style={[styles.input,   emailError ? styles.inputError : null]}
                    placeholder="Email"
                    placeholderTextColor="#888"
                    onChangeText={text => setEmail(text)}
                    value={email}
                />
                {emailError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{emailError}</Text>}
                <TextInput
                    style={[styles.input, passwordError ? styles.inputError : null]}
                    placeholder="Password"
                    placeholderTextColor="#888"
                    secureTextEntry
                    onChangeText={text => setPassword(text)}
                    value={password}
                />
                 {passwordError && <Text style={{color:'red',fontSize:15,marginLeft:"0%"}}>{passwordError}</Text>}
                <TouchableOpacity style={styles.signupButton} onPress={handleForgotPassword}>
                    <Text style={styles.forgotButtonText}>Forgot Password?</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={()=>validateForm()}>
                    <Text style={styles.buttonText}>Login</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.signupButton} onPress={handleSignup}>
                    <Text style={styles.signupButtonText}>Don't have an account? Signup</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>
    )
}
export default Login;

const styles=StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor:'white'
    },
    form:{
        marginTop:"-6%",
        paddingTop:30,
        backgroundColor:'#FFFFFF',
        borderTopStartRadius:30,
        borderTopEndRadius:30,
        width: '100%',
        alignItems: 'center',
    },
    input: {
        paddingVertical: windowHeight * 0.015,
        paddingHorizontal: windowWidth * 0.05,
        borderRadius: 10,
        marginTop: windowHeight * 0.03,
        width: windowWidth * 0.8,
        fontSize: windowWidth * 0.04,
        color: '#333',
        borderColor: '#ccc',
        borderWidth: 1,
    },
    button: {
        backgroundColor: '#187B75',
        padding: windowHeight * 0.016,
        borderRadius: 5,
        marginTop: windowHeight * 0.05,
        minWidth: windowWidth * 0.8,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: windowWidth * 0.05,
        textAlign: 'center',
    },
    signupButton: {
        marginTop: windowHeight * 0.05,
    },
    signupButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
    },
    forgotButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
        marginTop:-20,
    },
})