import React from 'react';
import { View } from 'react-native';
import { PieChart } from 'react-native-chart-kit';

const Speedometer = ({ value, total }) => {
  const difference = total - value;
  const data = [
    {
      name: 'Used',
      population: value,
      color: '#FF0000',
      legendFontColor: '#7F7F7F',
      legendFontSize: 12,
    },
    {
      name: difference >= 0 ? 'Remaining' : 'Extra Used',
      population: Math.abs(difference),
      color: difference >= 0 ? '#187B75' : '#FF8000',
      legendFontColor: '#7F7F7F',
      legendFontSize: 12,
    },
  ];

  return (
    <View style={{ flex: 1, justifyContent: 'center', alignItems: 'center', padding: 5 }}>
      <PieChart
        data={data}
        width={300}
        height={200}
        chartConfig={{
          color: (opacity = 1) => `rgba(255, 255, 255, ${opacity})`,
        }}
        accessor="population"
        backgroundColor="transparent"
        paddingLeft="15"
        absolute
      />
    </View>
  );
};

export default Speedometer;
