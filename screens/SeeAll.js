import React, { useState, } from 'react';
import { View, Text, StyleSheet, TouchableOpacity,Image,SafeAreaView,FlatList,ScrollView } from 'react-native';
import {
  KeyboardAvoidingView,
  TextInput,
} from 'react-native';
import Icon from 'react-native-vector-icons/Ionicons';
import { useNavigation } from '@react-navigation/native';
import {getAuth, createUserWithEmailAndPassword,signInWithEmailAndPassword,signOut} from 'firebase/auth';
import { collection, addDoc } from '@firebase/firestore';
import { doc, query, where, getDocs,  } from '@firebase/firestore';
import { db } from '../config';
import { useEffect} from "react";
import {firebase} from '../config';
import { useIsFocused } from '@react-navigation/native';

function MyExpense() {
  const navigation = useNavigation();
  const [searchText, setSearchText] = useState('');

  const [DATA, setDATA] = useState([]);
    
    const auth = getAuth()
    const today = new Date();
    const user = auth.currentUser;
    let month = today.getMonth() + 1;
    let year = today.getFullYear();

    const [data, setItemArray] = useState('');
    const [data2, setItemArray2] = useState('');
    const itemListCollection1 = firebase.firestore().collection('budget');
    const itemListCollection2 = firebase.firestore().collection('expense');

    useEffect ( () => {
      itemListCollection1
      .onSnapshot(
        querySnapshot => {
          const itemArray = []
          querySnapshot.forEach((doc) => {
            const {budget,month,user,year} = doc.data()
            itemArray.push({
              id: doc.id,
              user,
              budget,
              month,
              year
            })
          })
          setItemArray(itemArray)
        })
    },[])
  let budget

  useEffect ( () => {
      itemListCollection2
      .onSnapshot(
        querySnapshot => {
          const itemArray = []
          querySnapshot.forEach((doc) => {
            const {day,month,time,user,expenseName,expenseAmount,expenseDes} = doc.data()
            itemArray.push({
              id: doc.id,
              day,
              month,
              year,
              time,
              user,
              expenseName,
              expenseAmount,
              expenseDes
            })
          })
          setItemArray2(itemArray)
        })
    },[])

  let expenseTotal = Number("0");

  for (let i = 0; i < data.length; i++) {
      if(user.uid == data[i].user){
          if(month==data[i].month && year==data[i].year){
              budget = data[i].budget;
          }
      }
  }

  for (let i = 0; i < data2.length; i++) {
      if(user.uid == data2[i].user){
          if(month==data2[i].month && year==data2[i].year){
              expenseTotal = expenseTotal + Number(data2[i].expenseAmount)
          }
      }
  }

  var data3 = [];      
  for (let i = 0; i < data2.length; i++) {
      if(user.uid == data2[i].user){
          data3.push(data2[i])
          // console.log(data2[i])
      }
  }

  const searchAlgo = (search) => {
    var data4 = []
    // console.log(data3)

        if (search == '') { 
            data3.forEach(element2 => {
                data4.push(element2)
            })
        } 
        else if (!search) { 
          data3.forEach(element2 => {
              data4.push(element2)
          })
      } 
        else {
          data3.forEach(element => {
              if (String(element.expenseName).includes(search)) {
                data4.push(element)
              }
              else if (String(element.expenseAmount).includes(search)) {
                data4.push(element)
              }
            });
        }            

    setDATA(data4)
  }

  const handleGoBack = () => {
    // Handle navigation to previous page here
    navigation.goBack();
  };

  const handleSearch = () => {
    // Perform search logic here using the searchText state value
    console.log('Search for:', searchText);
    searchAlgo(searchText)
    // Implement the search functionality according to your requirements
  };

  const handleClearSearch = () => {
    setSearchText('');
  };

  const Item = ({item, itemId =item.id}) => (
    <TouchableOpacity onPress={()=>navigation.navigate('ExpenseDetails',{itemId})}>
        <View style={styles.Expenselist}>
            <Text style={styles.expenseName}>{item.expenseName}</Text>
            <Text style={styles.expensePrice}>{item.expenseAmount}</Text>
        </View>
    </TouchableOpacity>
  );

const renderItem = ({ item }) => {

    return (
      <Item
        item={item}
      />
    );
  };

  const isFocused = useIsFocused();
  useEffect(() => {
    console.log("updating")
    setDATA(data3)
  }, [isFocused]);

  useEffect(() => {
    handleSearch('')
  }, [searchText]);
  

  return (
    <KeyboardAvoidingView style={styles.container}>
      <View style={styles.nav}>
        <TouchableOpacity onPress={handleGoBack} style={styles.backButton}>
          <Icon name="arrow-back" size={24} color="white" />
        </TouchableOpacity>
        
        <Text style={{ textAlign: 'center', fontSize: 22, fontWeight: '500', color: 'white' }}>My Expense</Text>
        <View style={styles.searchContainer}>
          <View style={styles.searchInputContainer}>
            <TextInput
              style={styles.searchInput}
              placeholder="Search"
              value={searchText}
              onChangeText={text => setSearchText(text)}
            />
            {searchText !== '' && (
              <TouchableOpacity style={styles.clearButton} onPress={handleClearSearch}>
                <Icon name="close" size={18} color="#187B75" />
              </TouchableOpacity>
            )}
          </View>
        </View>
      </View>
      <View >
                {DATA.length==0&&
                <View style={styles.Expenselist1}>
                    <Text style={styles.expenseName}>No Results Found</Text>
                </View>

                }
                {DATA&&<FlatList
                    data={DATA}
                    renderItem={renderItem}
                    keyExtractor={(item) => item.id}
                />}
            </View>
    </KeyboardAvoidingView>

  );
}

export default MyExpense;
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  nav: {
    height: 160,
    backgroundColor: '#187B75',
    display: 'flex',
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    borderBottomLeftRadius: 20,
    borderBottomRightRadius: 20,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation: 10,
    paddingTop: '15%',
  },
  backButton: {
    position: 'absolute',
    top: 49,
    left: 25,
  },
  title: {
    textAlign: 'center',
    fontSize: 22,
    fontWeight: '500',
    color: 'white',
  },
  searchContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10,
    marginTop: 10,
    padding: 15,
  },
  searchInputContainer: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    backgroundColor: 'white',
    borderRadius: 10,
    paddingHorizontal: 10,
  },
  searchInput: {
    flex: 1,
    height: 40,
    color: 'black',
    marginLeft: 10,
  },
  clearButton: {
    padding: 5,
  },
  buttonGap: {
    width: 10, // Adjust the gap width as needed
  },
  searchButton: {
    padding: 10,
    backgroundColor: 'white',
    borderRadius: 10,
  },
  Expenselist:{
    // borderWidth:1,
    marginTop:'5%',
    height:62,
    width:'90%',
    marginLeft:'5%',
    borderRadius:10,
    backgroundColor:'#C8C8C8',
    display:'flex',
    flexDirection:'row',
    justifyContent:'center',
    alignItems:'center',
    gap:130,
    shadowColor: 'black',
    shadowOffset: { width: 0, height: 2 },
    shadowOpacity: 1,
    shadowRadius: 2,
    elevation:30
    },
    Expenselist1:{
      // borderWidth:1,
      marginTop:'30%',
      height:62,
      width:'90%',
      marginLeft:'5%',
      borderRadius:10,
      backgroundColor:'#C8C8C8',
      display:'flex',
      flexDirection:'row',
      justifyContent:'center',
      alignItems:'center',
      gap:130,
      shadowColor: 'black',
      shadowOffset: { width: 0, height: 2 },
      shadowOpacity: 1,
      shadowRadius: 2,
      elevation:30
      },
    expenseName:{
        fontSize:18,
        fontWeight:'500'
    },
    expensePrice:{
        fontSize:18,
        fontWeight:'400'
    },
});
