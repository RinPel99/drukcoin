import { useState } from 'react';
import HeaderPage from '../components/header';
import { View,Text,TextInput,StyleSheet,TouchableOpacity,Dimensions,KeyboardAvoidingView, Platform  } from 'react-native';
// import { Icon } from '@expo/vector-icons';
import { firebase } from '../config';

const windowWidth = Dimensions.get('window').width;
const windowHeight = Dimensions.get('window').height;

function ForgotPassword({navigation}){
    const [email, setEmail] = useState('');

    const handleVerify= () => {
        // handle login logic
        firebase.auth().sendPasswordResetEmail(email)
        .then(function() {
            // Password reset email sent successfully
            console.log("Password reset email sent.");
          })
          .catch(function(error) {
            // Error occurred while sending password reset email
            console.error(error);
          });
        navigation.navigate("Login")
    }
    const handleCancel= () => {
        // handle login logic
        navigation.navigate("Login")
    }
    return (
        <KeyboardAvoidingView style={styles.container}>
            <HeaderPage/>
            <View style={styles.form}>
                <Text style={{textAlign:'center',fontSize:27,fontWeight:'500'}}>Forgot Password</Text>
                <View style={{marginTop:'10%',justifyContent:"center",alignItems:"center"}}>
                    <Text style={{fontSize:20,color:'grey'}}>
                        Enter your email
                    </Text>
                    <Text style={{fontSize:20,color:'grey'}}>
                        to reset your password
                    </Text>
                </View>
                <TextInput
                    style={styles.input}
                    placeholder="YourEmai@gmail.com"
                    placeholderTextColor="#888"
                    onChangeText={text => setEmail(text)}
                    value={email}
                />
            
                <TouchableOpacity style={styles.button} onPress={()=>handleVerify()}>
                    <Text style={styles.buttonText}>Verify</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button} onPress={()=>handleCancel()}>
                    <Text style={styles.buttonText}>Cancel</Text>
                </TouchableOpacity>
            </View>
        </KeyboardAvoidingView>

    )
}
export default ForgotPassword;

const styles=StyleSheet.create({
    container: {
        flex: 1,
    },
    form:{
        marginTop:"-6%",
        paddingTop:30,
        backgroundColor:'#FFFFFF',
        borderTopStartRadius:30,
        borderTopEndRadius:30,
        width: '100%',
        alignItems: 'center',
    },
    input: {
        paddingVertical: windowHeight * 0.015,
        paddingHorizontal: windowWidth * 0.05,
        borderRadius: 10,
        marginTop: windowHeight * 0.03,
        width: windowWidth * 0.8,
        fontSize: windowWidth * 0.04,
        color: '#333',
        borderColor: '#ccc',
        borderWidth: 1,
    },
    button: {
        backgroundColor: '#187B75',
        padding: windowHeight * 0.016,
        borderRadius: 5,
        marginTop: windowHeight * 0.05,
        minWidth: windowWidth * 0.8,
    },
    buttonText: {
        color: '#fff',
        fontWeight: 'bold',
        fontSize: windowWidth * 0.05,
        textAlign: 'center',
    },
    signupButton: {
        marginTop: windowHeight * 0.05,
    },
    signupButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
    },
    forgotButtonText: {
        color: 'black',
        // fontWeight: 'bold',
        fontSize: windowWidth * 0.04,
        textAlign: 'center',
        marginTop:-20,
    },
})