import { createStackNavigator } from '@react-navigation/stack';
import LandingPage from '../screens/LandingPage';
import LoginPage from "../screens/LoginPage"
import SignupPage from "../screens/SignupPage"
import MyTabs from './Tab';
import MyExpnse from '../screens/SeeAll';
import ForgotPassword from '../screens/ForgotPassword';
import AddExpense from "../screens/AddExpense"
import BudgetSetPage from "../screens/SetBudget"
import ExpenseDetails from '../screens/ExpenseDetails';

const Stack = createStackNavigator();

function StackNavigation(){
    return(
        <Stack.Navigator>
        <Stack.Screen name="LandingPage" component={LandingPage} options={{ headerShown: false }}/>
        <Stack.Screen name="Login" component={LoginPage} options={{ headerShown: false }}/>
        <Stack.Screen name="Signup" component={SignupPage} options={{ headerShown: false }}/>
        <Stack.Screen name="ForgotPassword" component={ForgotPassword} options={{ headerShown: false }}/>
        <Stack.Screen name ="MyExpense" component={MyExpnse} options={{headerShown:false}}/>
        <Stack.Screen name ="AddExpense" component={AddExpense} options={{headerShown:false}}/>
        <Stack.Screen name="SetBudget" component={BudgetSetPage} options={{headerShown:false}}/>
        <Stack.Screen name ="Tab" component={MyTabs} options={{headerShown:false}}/>
        <Stack.Screen name="ExpenseDetails" component={ExpenseDetails} options={{headerShown:false}}/>
      </Stack.Navigator>
    )
}

export default StackNavigation;